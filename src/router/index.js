import Vue from 'vue';
import Router from 'vue-router';
import Waiver from '@/components/Waiver';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Waiver',
      component: Waiver,
    },
  ],
});
